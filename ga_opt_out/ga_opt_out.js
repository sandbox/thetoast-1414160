/*
 * @file
 * Javascript file for GA Opt Out module.
 *
 */


/*
 * Return null or the matched cookie name
 */ 
function gaOptOutGetCookie(check_name) {
    var a_all_cookies = document.cookie.split(';'),
    	a_temp_cookie = '',
		cookie_name = '',
    	cookie_value = '',
        b_cookie_found = false;

    for (i = 0; i < a_all_cookies.length; i++) {
        a_temp_cookie = a_all_cookies[i].split('=');

        // and trim left/right whitespace while we're at it
        cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, '');

        // if the extracted name matches passed check_name
        if (cookie_name == check_name) {
            b_cookie_found = true;
            // we need to handle case where cookie has no value but exists (no = sign, that is):
            if (a_temp_cookie.length > 1) {
                cookie_value = unescape(a_temp_cookie[1].replace(/^\s+|\s+$/g, ''));
            }
            // note that in cases where cookie is initialized but no value, null is returned
            return cookie_value;
            break;
        }
        a_temp_cookie = null;
        cookie_name = '';
    }
    
    if (!b_cookie_found) {
      return null;
    }
}

/*
 *  Clear cookies
 */ 
function gaOptOutClearCookie(name, domain, path){	
    if (gaOptOutGetCookie(name)) {
    	
        var domain = domain || document.domain; //this will fail with subdomains, use the domain from admin settings
        var path = path || "/";
        document.cookie = name + "=; expires=" + new Date + "; domain=" + domain + "; path=" + path;

    }    
};

/*
 * Return null or the cookie value of the checked cookie name
 */
function gaOptOutReadCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	
	for (var i = 0; i < ca.length; i++) {
	    var c = ca[i];
	
	    while (c.charAt(0) ==' ') {
	        c = c.substring(1, c.length);
	    }
	    if (c.indexOf(nameEQ) == 0) {
	        return c.substring(nameEQ.length, c.length);
	    }
	  }
	
	return null;
}
  
/*
 * Set a cookie
 */
function gaOptOutSetCookie(cookie_name, cookie_value, nDays) {
	var today = new Date();
	var expire = new Date();
	
	if (nDays==null || nDays==0) nDays=1;
	
	expire.setTime(today.getTime() + 3600000*24*nDays);  
	document.cookie = cookie_name+"="+escape(cookie_value)+ ";expires="+expire.toGMTString()+"; path=/";
}

/*
 * Called on page loading to set the default cookie if it doesn't exist 
 */
function gaOptOutInitCookie() {
	//create the cookie if it doesn't exist and set a default expire, it will change to the admin setting later on
	if (gaOptOutReadCookie('ga_opt_out') == null) {
		gaOptOutSetCookie('ga_opt_out', 'unset', 365);
	}
}
  
// Fire it up
gaOptOutInitCookie();

(function ($) {
	
	Drupal.behaviors.ga_opt_out = {
		attach: function (context, settings) {
		
			//get the domain from the admin settings, used to remove ga cookies
			var ga_opt_out_domain = Drupal.settings.ga_opt_out.ga_opt_out_domain;			

			//the user has yet to accept and visible is true, show the message
			if (gaOptOutReadCookie('ga_opt_out') == 'unset' && Drupal.settings.ga_opt_out.ga_opt_out_visible == true) {

				//reset the cookie with the expire value from drupal settings
				gaOptOutSetCookie('ga_opt_out', 'unset', parseInt(Drupal.settings.ga_opt_out.ga_opt_out_expire));			  

				//delete ga cookies if there are any
				gaOptOutClearCookie('__utma','.'+ga_opt_out_domain,'/');
				gaOptOutClearCookie('__utmb','.'+ga_opt_out_domain,'/');
				gaOptOutClearCookie('__utmc','.'+ga_opt_out_domain,'/');
				gaOptOutClearCookie('__utmz','.'+ga_opt_out_domain,'/');
				
				//message
				var message_container = '<div id="ga-opt-out-container">';
				message_container += '<div id="ga-opt-out-message-container" class="clearfix">';
				message_container += Drupal.settings.ga_opt_out.ga_opt_out_message;
				if(Drupal.settings.ga_opt_out.ga_opt_out_more_info) {
					message_container += '&nbsp;' + '<a id="ga-opt-out-more-info" href="' +  Drupal.settings.ga_opt_out.ga_opt_out_more_info + '" alt="More information" title="More information" rel="nofollow">More information.</a>';					
				}				
				message_container += '</div>';
				message_container += '<div id="ga-opt-out-buttons" class="clearfix">';
				message_container += '<div id="ga-opt-out-yes">Yes</div>';
				message_container += '<div id="ga-opt-out-no">No</div>';
				message_container += '</div>';								
				message_container += '</div>';
				
				$('body').append(message_container);
				  
				$('#ga-opt-out-container').delay(1500).slideDown();
				
				//accepted
				$('#ga-opt-out-yes').bind('click', function() {
					gaOptOutSetCookie('ga_opt_out', 'accept', parseInt(Drupal.settings.ga_opt_out.ga_opt_out_expire));
					$('#ga-opt-out-container').slideUp();
				});
				
				//declined
				$('#ga-opt-out-no').bind('click', function() {
					gaOptOutSetCookie('ga_opt_out', 'decline', parseInt(Drupal.settings.ga_opt_out.ga_opt_out_expire));
					$('#ga-opt-out-container').slideUp();         
				});
			
			}
			
			//remove ga cookies if declined and visible is true, otherwise leave the ga cookies as they are
			if (gaOptOutReadCookie('ga_opt_out') == 'decline' && Drupal.settings.ga_opt_out.ga_opt_out_visible == true) {
				//delete ga cookies if there are any
				gaOptOutClearCookie('__utma','.'+ga_opt_out_domain,'/');
				gaOptOutClearCookie('__utmb','.'+ga_opt_out_domain,'/');
				gaOptOutClearCookie('__utmc','.'+ga_opt_out_domain,'/');
				gaOptOutClearCookie('__utmz','.'+ga_opt_out_domain,'/');
			
			}			
		}
	};

}(jQuery));